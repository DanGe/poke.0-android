package org.stwingtep.poke0.app;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import net.openspatial.OpenSpatialService;

public class MainActivity extends Activity implements OpenSpatialService.OpenSpatialServiceCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override public void deviceConnected(BluetoothDevice bluetoothDevice) {
        // TODO unimplemented
    }

    @Override public void deviceDisconnected(BluetoothDevice bluetoothDevice) {
        // TODO unimplemented
    }

    @Override public void buttonEventRegistrationResult(BluetoothDevice bluetoothDevice, int i) {
        // TODO unimplemented
    }

    @Override public void pointerEventRegistrationResult(BluetoothDevice bluetoothDevice, int i) {
        // TODO unimplemented
    }

    @Override public void pose6DEventRegistrationResult(BluetoothDevice bluetoothDevice, int i) {
        // TODO unimplemented
    }

    @Override public void gestureEventRegistrationResult(BluetoothDevice bluetoothDevice, int i) {
        // TODO unimplemented
    }

    @Override public void motion6DEventRegistrationResult(BluetoothDevice bluetoothDevice, int i) {
        // TODO unimplemented
    }
}
